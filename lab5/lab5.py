import math

EPS = 0.001
BEGIN_POINT = 8
END_POINT = 10

def Ln(x):
    return math.log(x, math.exp(1))

def GetValueFunc(point):
    return math.cos(point/2)*Ln(point-1)

def GetX(a, b, func):
    return a - (func(a)/(func(b) - func(a)))*(b-a)

def main():
    iteration = 2
    temp = GetX(BEGIN_POINT, END_POINT, lambda x: math.cos(x/2)*Ln(x-1))
    x = GetX(BEGIN_POINT, temp, lambda x: math.cos(x/2)*Ln(x-1))
    while math.fabs(temp - x) > EPS:
        temp = x
        x = GetX(BEGIN_POINT, temp, lambda x: math.cos(x/2)*Ln(x-1))
        iteration+=1
    print(iteration)
    print(x)

if __name__ == '__main__':
    main()
