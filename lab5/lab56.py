import math
import time

EPS = 0.001
BEGIN_POINT = 0.7
END_POINT = 0.5

def GetY(x):
    return math.sqrt((1-x**2)/2)

def GetX(x, y):
    return math.sqrt(math.tan(x*y+0.1))

def main():
    iteration = 2
    tempX = GetY(BEGIN_POINT)
    tempY = GetX(BEGIN_POINT, END_POINT)
    y = GetY(tempX)
    x = GetX(tempX, tempY)
    while math.fabs(tempX - x) > EPS:
        iteration += 1
        tempX = x
        tempY = y
        y = GetY(tempX)
        x = GetX(tempX, tempY)
    print(x)
    print(y)
    print(iteration)

if __name__ == '__main__':
    main()
