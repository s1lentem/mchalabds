import math

BEGIN_X = 0.7
BEGIN_Y = 0.5
EPS = 0.001

YAKOBIAN = [[lambda x, y: -2*x+y*(math.tan((x*y+0.1)**2+1)),   lambda x, y: x*math.tan((x*y+0.1)**2 +1)],
            [lambda x: 2*x,                                    lambda y: 4*y  ]]

SYSTEM_OF_EQUATIONS = [ lambda x, y: math.tan(x*y+0.1) - x**2,
                        lambda x, y: x**2 + 2*y**2 - 1]

def ValueFromYakobian(x, y, yakobian):
    return yakobian[0][0](x, y) * yakobian[1][1](y) - yakobian[0][1](x, y) * yakobian[1][0](x)

def main():
    iteration = 2
    j = ValueFromYakobian(BEGIN_X, BEGIN_Y, YAKOBIAN)
    temp = SYSTEM_OF_EQUATIONS[0](BEGIN_X, BEGIN_Y) * YAKOBIAN[1][1](BEGIN_Y) - YAKOBIAN[0][1](BEGIN_X, BEGIN_Y) * SYSTEM_OF_EQUATIONS[1](BEGIN_X, BEGIN_Y)
    tempX = BEGIN_X - (1/j) *temp
    temp = YAKOBIAN[0][0](BEGIN_X, BEGIN_Y) * SYSTEM_OF_EQUATIONS[1](BEGIN_X, BEGIN_Y) - SYSTEM_OF_EQUATIONS[0](BEGIN_X, BEGIN_Y) * YAKOBIAN[1][0](BEGIN_X)
    tempY = BEGIN_Y - (1/j)*temp
    temp = SYSTEM_OF_EQUATIONS[0](tempX, tempY) * YAKOBIAN[1][1](tempY) - YAKOBIAN[0][1](tempX, tempY) * SYSTEM_OF_EQUATIONS[1](tempX, tempY)
    x = tempX - (1/j)*temp
    temp = YAKOBIAN[0][0](tempX, tempY) * SYSTEM_OF_EQUATIONS[1](tempX, tempY) - SYSTEM_OF_EQUATIONS[0](tempX, tempY) * YAKOBIAN[1][0](tempX)
    y = tempY - (1/j)*temp
    while math.fabs(tempX - x) > EPS:
        tempX = x
        tempY = y
        temp = SYSTEM_OF_EQUATIONS[0](tempX, tempY) * YAKOBIAN[1][1](tempY) - YAKOBIAN[0][1](tempX, tempY) * SYSTEM_OF_EQUATIONS[1](tempX, tempY)
        x = tempX - (1/j)*temp
        temp = YAKOBIAN[0][0](tempX, tempY) * SYSTEM_OF_EQUATIONS[1](tempX, tempY) - SYSTEM_OF_EQUATIONS[0](tempX, tempY) * YAKOBIAN[1][0](tempX)
        y = tempY - (1/j)*temp
        iteration += 1
    print(x)
    print(y)
    print(iteration)


if __name__ == '__main__':
    main()
