import math

EPS = 0.001
START_POINT = 8

def Ln(x):
    return math.log(x, math.exp(1))

def GetValueFunc(x):
    return math.cos(x/2)*Ln(x-1)

def GetValueDiffFunc(x):
    return -(1/2)*(Ln(x-1))*math.sin(x/2)+(math.cos(x/2)/(x-1))

def GetX(prevX, func, difFunc):
    return prevX - func(prevX)/difFunc(prevX)

def main():
    iteration = 2
    temp = GetX(START_POINT, GetValueFunc, GetValueDiffFunc)
    x = GetX(temp, GetValueFunc, GetValueDiffFunc)
    while math.fabs(temp - x) > EPS:
        temp = x
        x = GetX(temp, GetValueFunc, GetValueDiffFunc)
        iteration += 1
    print(iteration)
    print(x)

if __name__ == '__main__':
    main()
