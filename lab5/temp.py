import math

BEGIN_X = 1.2
BEGIN_Y = 1.7
EPS = 0.001

YAKOBIAN =  [[lambda x: 6*x**2,                 lambda y: -2*y],
            [lambda y: y**3,                    lambda x, y: 3*x*y**2-1  ]]

SYSTEM_OF_EQUATIONS = [ lambda x, y: 2*x**3-y**2 - 1,
                        lambda x, y: x*y**3-y-4]


def ValueFromYakobian(x, y, yakobian):
    return yakobian[0][0](x) * yakobian[1][1](x, y) - yakobian[0][1](y) * yakobian[1][0](y)



def PowCos(value):
    return (1+math.cos(2*value))/2

def PrintMatrix(matrix):
    for i in range(len(matrix)):
        for j in range(len(matrix[i])):
            print(str(matrix[i][j]) + ' ', end='')
        print()

def main():
    iteration = 2
    j = ValueFromYakobian(BEGIN_X, BEGIN_Y, YAKOBIAN)
    temp = SYSTEM_OF_EQUATIONS[0](BEGIN_X, BEGIN_Y) * YAKOBIAN[1][1](BEGIN_X, BEGIN_Y) - YAKOBIAN[0][1](BEGIN_Y) * SYSTEM_OF_EQUATIONS[1](BEGIN_X, BEGIN_Y)
    tempX = BEGIN_X - (1/j) *temp
    temp = YAKOBIAN[0][0](BEGIN_X) * SYSTEM_OF_EQUATIONS[1](BEGIN_X, BEGIN_Y) - SYSTEM_OF_EQUATIONS[0](BEGIN_X, BEGIN_Y) * YAKOBIAN[1][0](BEGIN_Y)
    tempY = BEGIN_Y - (1/j)*temp
    j = ValueFromYakobian(tempX, tempY, YAKOBIAN)
    temp = SYSTEM_OF_EQUATIONS[0](tempX, tempY) * YAKOBIAN[1][1](tempX, tempY) - YAKOBIAN[0][1](tempY) * SYSTEM_OF_EQUATIONS[1](tempX, tempY)
    x = tempX - (1/j)*temp
    temp = YAKOBIAN[0][0](tempX) * SYSTEM_OF_EQUATIONS[1](tempX, tempY) - SYSTEM_OF_EQUATIONS[0](tempX, tempY) * YAKOBIAN[1][0](tempY)
    y = tempY - (1/j)*temp
    while math.fabs(tempX - x) > EPS:
        tempX = x
        tempY = y
        j = ValueFromYakobian(tempX, tempY, YAKOBIAN)
        temp = SYSTEM_OF_EQUATIONS[0](tempX, tempY) * YAKOBIAN[1][1](tempX, tempY) - YAKOBIAN[0][1](tempY) * SYSTEM_OF_EQUATIONS[1](tempX, tempY)
        x = tempX - (1/j)*temp
        temp = YAKOBIAN[0][0](tempX) * SYSTEM_OF_EQUATIONS[1](tempX, tempY) - SYSTEM_OF_EQUATIONS[0](tempX, tempY) * YAKOBIAN[1][0](tempY)
        y = tempY - (1/j)*temp
        iteration += 1
        print(x)
        print(y)
        print()
    print(x)
    print(y)
    print(iteration)


if __name__ == '__main__':
    main()
