import numpy
from math import sqrt

def PrintMatrix(matrix, size):
    for i in range(size):
        print(matrix[i])

def GetTMatrix(matrix, size):
    return [[matrix[i][j] for i in range(size)] for j in range(size)]

def MultMatrix(matrix1, matrix2):
    temp1 = numpy.array(matrix1)
    temp2 = numpy.array(matrix2)
    return list(numpy.dot(temp1, temp2))

def GetDetA(matrix, size):
    result = 1
    for i in range(size):
        result *= matrix[i][i]**2
    return result

# 1-й Результат из транспонированной матрицы
def GetTempResult(matrix, size, results):
    newResult = list()
    for i in range(size):
        temp = 0
        for j in range(i):
            temp += matrix[i][j] * newResult[j]
        newResult.append((results[i]-temp)/matrix[i][i])
    return newResult

# 2-й финальный результат 
def GetResult(matrix, size, results):
    newResult = list()
    for i in range(size-1, -1, -1):
        temp = 0
        for j in range(size-1, i, -1):
            temp += matrix[i][j] * newResult[size-j-1]
        newResult.append((results[i] - temp)/matrix[i][i])
    return newResult

# При i = 1
def GetNewMatrix(matrix, size):
    result = [[0 for i in range(size)] for j in range(size)]
    for i in range(size):
        for j in range(i,size):
            if i == 0:
                if j == i:
                    result[i][j] = sqrt(matrix[i][j])
                else:
                    result[i][j] = matrix[i][j]/result[i][i]
            else:
                if i == j:
                    temp = 0
                    for k in range(j):
                        temp += result[k][j] ** 2
                    result[i][j] = sqrt(matrix[i][j] - temp)
                else:
                    temp = 0
                    for k in range(j):
                        temp += result[k][i]*result[k][j]
                    result[i][j] = (matrix[i][j] - temp)/result[i][i]
    return result
