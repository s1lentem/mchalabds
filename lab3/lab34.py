from funcs import *
from const import *

def main():
    tempMatrix = MultMatrix(MATRIX, GetTMatrix(MATRIX, SIZE))
    newMatrix = GetNewMatrix(tempMatrix, SIZE)
    newTranspMatrix = GetTMatrix(newMatrix, SIZE)
    elemMainMatrix = list()
    for i in range(SIZE):
        tempRes = [1 if j == i else 0 for j in range(SIZE)]
        temp = GetTempResult(newTranspMatrix, SIZE, tempRes)
        elemMainMatrix.append(GetResult(newMatrix, SIZE, temp))
    PrintMatrix(elemMainMatrix[::-1], SIZE)


if __name__ == '__main__':
    main()
