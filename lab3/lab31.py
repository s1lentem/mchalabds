from const import *
from funcs import *

def main():
    PrintMatrix(MultMatrix(MATRIX, GetTMatrix(MATRIX, SIZE)), SIZE)


if __name__ == '__main__':
    main()
