from funcs import *
from const import *

def main():
    tempMatrix = MultMatrix(MATRIX, GetTMatrix(MATRIX, SIZE))
    newMatrix = GetNewMatrix(tempMatrix, SIZE)
    determinant = GetDetA(newMatrix, SIZE)
    print(determinant)


if __name__ == '__main__':
    main()
