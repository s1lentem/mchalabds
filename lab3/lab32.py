from funcs import *
from const import *

def main():
    tempMatrix = MultMatrix(MATRIX, GetTMatrix(MATRIX, SIZE))
    newRes = MultMatrix(RESULT, GetTMatrix(MATRIX, SIZE))
    #print(newRes, end='\n\n')
    newMatrix = GetNewMatrix(tempMatrix, SIZE)
    newTranspMAtrix = GetTMatrix(newMatrix, SIZE)
    tempRes = GetTempResult(newTranspMAtrix, SIZE, newRes)
    #print(tempRes)
    PrintMatrix(newMatrix, SIZE)
    res = GetResult(newMatrix, SIZE, tempRes)
    print(res)


if __name__ == '__main__':
    main()
