from const import MATRIX
from const import SIZE
from funcs import PrintMatrix
from math import fabs

def CheckConvergMethods(matrix, size):
    mainElements = [fabs(matrix[i][i]) for i in range(size)]
    newMatrix = [[fabs(matrix[i][j]) for i in range(size) if i != j] for j in range(size)]
    allSums = [sum(newMatrix[i]) for i in range(size)]
    result = [0 for i in range(size) if mainElements[i] > allSums[i]]
    return len(result) == size

def main():
    result = CheckConvergMethods(MATRIX, SIZE)
    PrintMatrix(MATRIX, SIZE)
    if result:
        print('Методы сходится')
    else:
        print('Методы не сходится')

if __name__ == '__main__':
    main()
