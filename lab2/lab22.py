from funcs import *
from const import *

def Iter(matrix, res):
    tempRes = [0] * 4
    for i in range(len(matrix)):
        for j in range(len(matrix[i])-1):
            temp = j if j < i else j + 1
            tempRes[i] += matrix[i][j] * res[temp]
        tempRes[i] += matrix[i][-1]
        res[i] = tempRes[i]
    return tempRes

def main():
    iteration = 1
    tempSumm = [0] * SIZE
    newMatrix = GetNormalMatrix(MATRIX, RESULT, SIZE)
    prevSumm = Iter(newMatrix, tempSumm)
    norm = GetNorm(newMatrix)
    while True:
        iteration += 1
        tempSumm = Iter(newMatrix, prevSumm)
        if CheckDelta(tempSumm, prevSumm, norm, SIZE, EPS):
            break
        prevSumm = tempSumm.copy()
    PrintResult(iteration, tempSumm)

if __name__ == '__main__':
    main()
