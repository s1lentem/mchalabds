from math import fabs

def PrintResult(iteration, results):
    print('Iteration: ' +    str(iteration))
    print('Result: ', end=' ')
    print(results)

def PrintMatrix(matrix, size):
    for i in range(size):
        print(matrix[i])

def GetNorm(matrix):
    result = [0]*4
    for i in range(len(matrix)):
        for j in range(len(matrix[i])-1):
            result[i] += fabs(matrix[i][j])
    return max(result)

def GetNormalMatrix(matrix, result, size):
    temp = list()
    newArr = matrix.copy()
    for i in range(size):
        temp.append(matrix[i][i])
        matrix[i].pop(i)
    for i in range(size):
        newArr[i].append(result[i]*(-1))
        for j in range(len(newArr[i])):
            newArr[i][j] *= -1
            newArr[i][j] /= temp[i]
    return newArr

def CheckDelta(current, prev, norm, size, eps):
    result = max([fabs(current[i] - prev[i]) for i in range(size)])
    delta = (1 - norm)/norm*eps
    return result < delta
