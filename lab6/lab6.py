# from matplotlib import pyplot as plt
# import numpy as np


ALL_X = [0.132, 0.567, 1.153, 2.414, 3.939]
ALL_Y = [69.531, 1.112, -1.672, -1.922, -1.925]

def print_matrix(matrix):
    for i in range(len(matrix)):
        print(matrix[i])

def get_temp2(index, all_x, all_y):
    res = 1
    for i in range(len(all_x)):
        if i == index:
            continue
        temp = all_x[index] - all_x[i]
        res *= temp
    return all_y[index] / res

def mult_2x(x1, x2, y1, y2):
    return [x1*y1, x1*y2 + x2*y1, x2*y2]

def mult_3x(x1, x2, x3, y1, y2, y3):
    return [x1*y1, x1*y2 + x2*y1, x1*y3 + x2*y2 + x3*y1, x2*y3 + x3*y2, x3*y3]

def get_temp1(all_x):
    res = list()
    for i in range(len(all_x)):
        if i == 0:
            temp1 = mult_2x(1, all_x[1], 1, all_x[2])
            temp2 = mult_2x(1, all_x[3], 1, all_x[4])
        elif i == 1:
            temp1 = mult_2x(1, all_x[0], 1, all_x[2])
            temp2 = mult_2x(1, all_x[3], 1, all_x[4])
        elif i == 2:
            temp1 = mult_2x(1, all_x[0], 1, all_x[1])
            temp2 = mult_2x(1, all_x[3], 1, all_x[4])
        elif i == 3:
            temp1 = mult_2x(1, all_x[0], 1, all_x[1])
            temp2 = mult_2x(1, all_x[2], 1, all_x[4])
        elif i == 4:
            temp1 = mult_2x(1, all_x[0], 1, all_x[1])
            temp2 = mult_2x(1, all_x[2], 1, all_x[3])
        res.append(mult_3x(temp1[0], temp1[1], temp1[2], temp2[0],
                           temp2[1], temp2[2]))
    return res

def get_y(func, x):
    return func[4]*x**4 - func[3]*x**3 + func[2]*x**2 - func[1]*x + func[0]

def multiply_polynomial_by_number(poly, num):
    return [item * num for item in poly]

def get_l(temp1, all_x, all_y):
    alls = list()
    res = [0, 0, 0, 0, 0]
    for i in range(5):
        temp = multiply_polynomial_by_number(temp1[i], get_temp2(i, all_x, all_y))
        alls.append(temp)
    for i in range(5):
        for j in range(5):
            res[j] += alls[i][j]
    return res
# path 2
#-------------------------------------------------------------------------------
def get_step(all_x):
    res = 0
    for i in range(1, len(all_x)):
        res += all_x[i] - all_x[i-1]
    return res/4

def get_xk(all_x, step):
    res = list()
    for i in range(len(all_x)):
        res.append(all_x[0] + i * step)
    return res

def get_line_y(x, k, b):
    return k * x + b

def get_difference(func, x1, x2):
    return (func(x2) - func(x1))/(x2 - x1)

def get_coll_for_first_table(all_y):
    res = list()
    for i in range(1, len(all_y)):
        res.append(all_y[i] - all_y[i-1])
    return res

def get_firs_table(all_y):
    table = list()
    values = all_y
    for i in range(4):
        table.append(get_coll_for_first_table(values))
        values = table[i]
    return table

def get_coll_for_second_table(all_x, all_old_x, delta):
    res = list()
    for i in range(1, len(all_x)):
        res.append((all_x[i] - all_x[i-1]) / (all_old_x[i+delta] - all_old_x[i-1]))
    return res


def get_second_table(all_x, all_old_x):
    res = list()
    res.append(get_coll_for_second_table(all_x, all_old_x, 0))
    res.append(get_coll_for_second_table(res[0], all_old_x, 1))
    res.append(get_coll_for_second_table(res[1], all_old_x, 2))
    res.append(get_coll_for_second_table(res[2], all_old_x, 3))
    return res

#-------------------------------------------------------------------------------
# part 3
def mult2x_and_3x(x1, x2, y1, y2, y3):
    return [x1*y1, x1*y2 + x2*y1, x1*y3 + x2*y2, x2*y3]

def get_poly(all_x, y0, all_f1):
    f1 = multiply_polynomial_by_number([1, all_x[0]], all_f1[0])
    f2 = multiply_polynomial_by_number(mult_2x(1, all_x[0], 1, all_x[1]),
                                       all_f1[1])
    temp = mult_2x(1, all_x[0], 1, all_x[1])
    f3 = multiply_polynomial_by_number(mult2x_and_3x(1, all_x[2], temp[0],
                                                    temp[1], temp[2]), all_f1[2])
    temp2 = mult_2x(1, all_x[2], 1 ,all_x[3])
    temp_res = mult_3x(temp[0], temp[1], temp[2], temp2[0], temp2[1], temp2[2])
    f4 = multiply_polynomial_by_number(temp_res, all_f1[3])
    result = [f4[0], f4[1] + f3[0], f4[2] + f3[1] + f2[0], f4[3] + f3[2] +
              f2[1], + f1[0] + f4[4] + f3[3] + f2[2] + f1[1] + y0]
    return result
#------------------------------------------------------------------------------
# part4

def get_b(all_f, all_a, all_x):
    res = list()
    for i in range(len(all_a)):
        res.append(all_f[i] - all_a[i]*all_x[i])
    return res

def get_a(all_y, all_x):
    res = list()
    for i in range(1,3):
        sum1 = (all_y[i+1] - all_y[i-1])/((all_x[i+1] - all_x[i-1])*(all_x[i+1] - all_x[i]))
        sum2 = (all_y[i] - all_y[i-1]) / ((all_x[i] - all_x[i-1])-(all_x[i+1] - all_x[i]))
        res.append(sum1 - sum2)
    return res

def get_second_b(all_y, all_x, all_a):
    res = list()
        for i in range(1, 3):
        temp1 = (all_y[i] - all_y[i-1])/(all_x[i] - all_x[i-1])
        temp2 = all_a[i-1] * (all_x[i] + all_x[i+1])
        res.append(temp1 - temp2)
    return res

def get_c(all_y, all_x, all_a, all_b):
    res = list()
    for i in range(1, 3):
        temp = all_y[i-1] - all_b[i-1]*all_x[i-1] - all_a[i-1]*all_x[i-1]**2
        res.append(temp)
    return res
#------------------------------------------------------------------------------
# 5 part

def get_all_comp_for_cube_spline(x, y):
    h = [0 for i in range(4)]
    l = [0 for i in range(5)]
    delta = [0 for i in range(4)]
    lam = [0 for i in range(4)]
    h = [0 for i in range(5)]
    c = [0 for i in range(5)]
    b = [0 for i in range(4)]
    d = [0 for i in range(4)]
    a = [0 for i in range(4)]
    a[:-1] = y [1:]
    for i in range(len(x) - 1):
        h[i] = x[i + 1] - x[i]
        l[i] = (y[i + 1] - y[i]) / h[i]

    delta[0] = - 1 / 2 * h[1] / (h[0] + h[1])
    lam[0] = 3 / 2 * (l[1] - l[0]) / (h[1] + h[0])

    for i in range(2, len(x)):
        delta[i - 1] = - h[i] / (2 * h[i - 1] + 2 * h[i] + h[i - 1] * delta[i - 2])

    for i in range(2, len(x)):
        lam[i - 1] = (2 * l[i] - 3 * l[i - 1] - h[i - 1] * lam[i - 2]) / (
        2 * h[i - 1] + 2 * h[i] + h[i - 1] * delta[i - 2])

    for i in range(len(x) - 1, 0, -1):
        c[i - 1] = delta[i - 1] * c[i] + lam[i - 1]

    for i in range(1, len(x) - 1):
        b[i] = l[i] + 2 / 3 * c[i] * h[i] + 1 / 3 * h[i] * c[i - 1]
        d[i] = (c[i] - c[i - 1]) / (3 * h[i])

    b[0] = l[0] + 2 / 3 * c[0] * h[0]
    d[0] = c[0] / (3 * h[0])
    return (a, b, c, d)

def print_all_comp_for_cube_spline(comp):
    print('a: ', end=' ')
    print(comp[0])
    print('b: ', end=' ')
    print(comp[1])
    print('c: ', end=' ')
    print(comp[2])
    print('d: ', end=' ')
    print(comp[3])

#------------------------------------------------------------------------------
def main():
    print('1)')
    temp1 = get_temp1(ALL_X)
    res = get_l(temp1, ALL_X, ALL_Y)
    print(res)

    print('2)')
    step = get_step(ALL_X)
    new_x = get_xk(ALL_X, step)

    temp = list()
    for value in ALL_Y:
        temp.append(get_y(res, value))
    first_table = get_firs_table(temp)
    print('First table:')
    print_matrix(first_table)
    second_table = get_second_table(temp, new_x)
    print('Second table:')
    print_matrix(second_table)
    print()
    print('3')
    all_f1 = [item[0] for item in second_table]
    polynom = get_poly(new_x, temp[0], all_f1)
    print(polynom)
    print(get_y(polynom, ALL_X[0] + ALL_X[1]))
    print()


    print(get_b(temp, second_table[0] ,new_x))
    print()
    a = get_a(temp, new_x)
    print(a)
    b = get_second_b(temp, new_x, a)
    print(b)
    c = get_c(temp, new_x, a, b)
    print(c)

    comp = get_all_comp_for_cube_spline(new_x, temp)
    print_all_comp_for_cube_spline(comp)

    # ypts_linear = []
    # xpts_linear = []
    # for i in range(1, len(ALL_X)):
    #     cur_xpts = np.arange(ALL_X[i - 1], ALL_X[i], 0.01)
    #     xpts_linear += list(cur_xpts)
    #     ypts_linear += [get_line_y(t, new_x[i - 1], temp[i - 1]) for t in cur_xpts]
    # plt.plot(xpts_linear, ypts_linear, label='Linear spline')

if __name__ == '__main__':
    main()
