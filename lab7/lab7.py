import math
import matplotlib.pyplot as pl
import pandas as pd

from funcs import *



A = 0
B = 1
FUNC = lambda x: x**2*math.sin(x)
ANTIDERIVATIVE_FUNC = lambda x: 2*x*math.sin(x) - (x**2 - 2)*math.cos(x)
SECOND_DERIVATIVE = lambda x: -(x**2)*math.sin(x) + 4*x*math.cos(x) * 2*math.sin(x)
FOURTH_DERIVATIVE = lambda x: math.sin(x) * (x**2-12) - 8 * x * math.cos(x)
EPS = 0.01

EPS_CAUCHY = 0.0001
FUNC_CAUCHY = lambda x, y: (3*y - 20*x**2*y**3 - 12*y**3)/(2*x)
RESULT = lambda x: x**(3/2)/math.sqrt(4*x**5+4*x**3+8)
A_CAUCHY = 1
B_CAUCHY = 5
INITIAL_CONDITION = 0.25


FIRST_VALUE = 0
SECOND_VALUE = 0


def first_task():
    print('FIRST TASK')
    step = get_step_with_accuracy(A, B, FUNC, EPS)
    print('STEP = {}'.format(step))
    print()

def second_task():
    print('SECOND TASK')
    n = get_step_with_accuracy(A, B, FUNC, EPS)
    step = get_step(A, B, n)
    first_value = get_integral_from_step(A, B, FUNC, step)
    second_value = get_integral_from_step(A, B, FUNC, 2*step)
    FIRST_VALUE = first_value
    print('FIRST VALUE = {}'.format(first_value))
    print('SECOND VALUE = {}'.format(second_value))
    error = second_error(A, B, SECOND_DERIVATIVE, step)
    print(error)
    print()

def third_task():
    print('THIRD TASK')
    n = get_step_with_accuracy(A, B, FUNC, EPS)
    step = step_from_simposn(A, B, n)
    first_value = method_simpsona(A, B, step, FUNC)
    second_value = method_simpsona(A, B, 2*step, FUNC)
    SECOND_VALUE = first_value
    print('FIRST VALUE = {}'.format(first_value))
    print('SECOND VALUE = {}'.format(second_value))
    error = third_error(A, B, SECOND_DERIVATIVE, step)
    print(error)
    print()

def fourth_task():
    print('FOURTH TASK')
    value = ANTIDERIVATIVE_FUNC(B)- ANTIDERIVATIVE_FUNC(A)
    print('VALUE = {}'.format(value))
    if math.fabs(value - FIRST_VALUE) < math.fabs(value - SECOND_VALUE):
        print('Method of trapeziums')
    else:
        print('The Simpson Method')

    print()

def fifth_task():
    print('FIFTH TASK')
    step = get_step_for_runge(EPS_CAUCHY)
    print('step = {}'.format(step))
    print()

def sixth_task():
    print('SIXTH TASK')
    step = get_step_for_runge(EPS_CAUCHY)
    value = method_runge_kutta(A_CAUCHY, B_CAUCHY, step,
                               INITIAL_CONDITION, FUNC_CAUCHY)
    value2 = method_adams(A_CAUCHY, B_CAUCHY, step, INITIAL_CONDITION,
                          FUNC_CAUCHY)
    x = get_range(A_CAUCHY, B_CAUCHY, step)
    pl.plot(x, value2, color='y')
    pl.plot(x, value, color='r')
    print('VALUE = {}'.format(value[-1]))
    print(len(value))
    print()
    return (x, value, value2)

def seventh_task():
    print('SEVENTH TASK')
    step = get_step_for_runge(EPS_CAUCHY)
    value = method_euler(A_CAUCHY, B_CAUCHY, step, INITIAL_CONDITION,
                         FUNC_CAUCHY)

    x = get_range(A_CAUCHY, B_CAUCHY, step)
    pl.plot(x, value, color='b')
    print(value[-1])
    print()
    return value

def eighth_task():
    print('EIGHTH TASK')
    step = get_step_for_runge(EPS_CAUCHY)
    all_x = get_range(A_CAUCHY, B_CAUCHY, step)
    ALL_X = all_x
    all_y = [RESULT(x) for x in all_x]
    pl.plot(all_x, all_y, color='g')
    return all_y

def all_table(all_x, runge, addams, euler, original):
    df_runge = pd.DataFrame({
        'xi': all_x,
        'yi': runge,
        '`yi`': original,
        'delta': [math.fabs(original[i] - runge[i]) for i in range(len(original))]
    })
    df_addams = pd.DataFrame({
        'xi': all_x,
        'yi': addams,
        '`yi`': original,
        'delta': [math.fabs(original[i] - addams[i]) for i in range(len(original))]
    })
    df_euler = pd.DataFrame({
        'xi': all_x,
        'yi': euler,
        '`yi`': original,
        'delta': [math.fabs(original[i] - euler[i]) for i in range(len(original))]
    })
    last_tabe = pd.DataFrame({
        'xi': all_x,
        '`yi`': original,
        'yi-Runne': runge,
        'delta-1': [math.fabs(original[i] - runge[i]) for i in range(len(original))],
        'yi-Addams': addams,
        'delta-2': [math.fabs(original[i] - addams[i]) for i in range(len(original))]
    })
    return (df_runge, df_addams, df_euler, last_tabe)


def main():
    first_task()
    second_task()
    third_task()
    fourth_task()
    fifth_task()
    temp1 = sixth_task()
    temp2 = seventh_task()
    temp3 = eighth_task()
    for table in all_table(temp1[0], temp1[1], temp1[2], temp2, temp3):
        print()
        print(table)
        print()
    pl.show()


if __name__ == '__main__':
    main()
