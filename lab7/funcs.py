import math
import numpy as np



def get_step(a, b, n):
    return (b-a)/n

def step_from_simposn(a, b, n):
    return (b-a)/(2*n)

def get_integral_from_step(a, b, func, step):
    x = a + step
    func_n = func(b)
    temp = 0
    while x < b:
        temp += func(x)
        x += step
    return (step/2)*(func(a) + 2*temp + func_n)

def get_integral(a, b, func, n):
    step = get_step(a, b, n)
    x = a + step
    f_n = func(b)
    temp = 0
    for i in range(1, n - 1):
        temp += func(x)
        x += step
    return  (step/2)*(func(a) + 2*temp + f_n)

def get_step_with_accuracy(a, b, func, eps):
    n = 4
    temp_integral = get_integral(a, b, func, n)
    n *= 2
    second_temp_integral = get_integral(a, b, func, n)
    while math.fabs(second_temp_integral - temp_integral) > eps:
        n *= 2
        temp_integral = second_temp_integral
        secon_temp_integral = get_integral(a, b, func, n)
    return n

def get_max_from_second_func(a  , b, step, func):
    all_x = np.arange(a, b, step)
    all_value = [math.fabs(func(x)) for x in all_x]
    return max(all_value)

def second_error(a, b, func, step):
    return get_max_from_second_func(a, b, step, func) * step**2 * (b-a) / 12

def third_error(a, b, func, step):
    return get_max_from_second_func(a, b, step, func) * step**4/180

def method_simpsona(a, b, step, func):
    all_x = np.arange(a, b, step)
    all_y = [func(x) for x in all_x]
    first_temp_sum = sum([all_y[i] for i in range(1, len(all_y), 2)])
    second_temp_sum = sum([all_y[i] for i in range(2, len(all_y), 2)])
    return (step/3)*(func(a) + 4*first_temp_sum + 2* second_temp_sum + func(b))

def get_step_for_runge(eps):
    return eps**(1/4)

def get_all_point(a, b, step):
    return list(np.arange(a, b, step))

def method_runge_kutta(a, b, step, y0, func):
    all_x = get_all_point(a, b, step)
    all_y = [y0]
    y = y0
    for x in all_x[1:]:
        k1 = func(x, y)
        k2 = func(x + step/2, y + step*k1/2)
        k3 = func(x + step/2, y + step*k2/2)
        k4 = func(x + step, y + step*k3)
        temp = y + step/6 * (k1 + 2*k2 + 2*k3 + k4)
        all_y.append(temp)
        y = temp
    return all_y

def method_adams(a, b, step, y0, func):
    all_x = get_all_point(a, b, step)
    all_y = [y0]
    temp = y0 + step * func(all_x[0],y0)
    all_y.append(temp)
    for i in range(1, len(all_x)):
        temp = all_y[i] + (step/2)*(3*func(all_x[i], all_y[i]) -
                                    func(all_x[i-1], all_y[i-1]))
        all_y.append(temp)
    return all_y[:-1]

def method_euler(a, b, step, y0, func):
    all_x = get_all_point(a, b, step)
    all_y = [y0]
    y = y0
    for x in all_x[1:]:
        temp = y + step * func(x, y)
        all_y.append(temp)
        y = temp
    return all_y

def point_cauchy(a, b, step, y0, func):
    all_x = get_all_point(a, b, step)
    all_y = [y0]
    for x in all_x[1:]:
        all_y.append(func(x))
    return all_y

def get_range(begin, end, step):
    return list(np.arange(begin, end, step))
