import numpy
from math import fabs, atan, sin, cos

def PrintMatrix(matrix, size):
    for i in range(size):
        print(matrix[i])

def GetTMatrix(matrix, size):
    return [[matrix[i][j] for i in range(size)] for j in range(size)]

def MultMatrix(matrix1, matrix2):
    temp1 = numpy.array(matrix1)
    temp2 = numpy.array(matrix2)
    return list(numpy.dot(temp1, temp2))

def GetMax(matrix, size):
    result = None
    for i in range(size):
            for j in range(i+1, size):
                if result is None:
                    result = (matrix[i][j], i , j)
                else :
                    if fabs(result[0]) < fabs(matrix[i][j]):
                        result = (matrix[i][j], i, j)
    return result

def GetAngleRotation(matrix, maxElement):
    i = maxElement[1]
    j = maxElement[2]
    return atan(2*matrix[i][j]/(matrix[i][i]- matrix[j][j]))/2

def GetMatixRotation(maxElement, angleRotation, size):
    result = [[1 if j == i else 0 for j in range(size)] for i in range(size)]
    for i in range(size):
        for j in range(size):
            if ((maxElement[1] == i and maxElement[1] == j) or (maxElement[2] == i and maxElement[2] == j)):
                result[i][j] = cos(angleRotation)
            if (maxElement[1] == i and maxElement[2] == j):
                result[i][j] = 0-sin(angleRotation)
            if (maxElement[2] == i and maxElement[1] == j):
                result[i][j] = sin(angleRotation)
    return result

def GetEigenvalues(matrix, size):
    return [matrix[i][i] for i in range(size)]

def GetEigenvectors(allUMatrix, iteration, matrix, size):
    result = allUMatrix.pop(0)
    for i in range(iteration-2):
        temp = allUMatrix.pop(0)
        result = MultMatrix(result, temp)
    return [[result[j][i] for j in range(size)] for i in range(size)]
