from funcs import *
from const import MATRIX, SIZE, EPS

def main():
    iteration = 0
    allUMatrix = []
    temp = GetTMatrix(MATRIX, SIZE)
    newMatrix = MultMatrix(MATRIX, temp)
    maxElement = GetMax(newMatrix, SIZE)
    while fabs(maxElement[0]) > EPS:
        angle = GetAngleRotation(newMatrix, maxElement)
        uMatrix = GetMatixRotation(maxElement, angle, SIZE)
        uTranspMatrix = GetTMatrix(uMatrix, SIZE)
        newMatrix = MultMatrix(MultMatrix(uTranspMatrix, newMatrix), uMatrix)
        maxElement = GetMax(newMatrix, SIZE)
        allUMatrix.append(uMatrix)
        iteration += 1
    print('Собственные значения:')
    print(GetEigenvalues(newMatrix, SIZE))
    print('Собстенные векторы:')
    oldMatrix = MultMatrix(MATRIX, temp)
    eigenvectors = GetEigenvectors(allUMatrix, iteration, oldMatrix, SIZE)
    for i in range(SIZE):
        print(i+1, end=") ")
        print(eigenvectors[i])

if __name__ == '__main__':
    main()
